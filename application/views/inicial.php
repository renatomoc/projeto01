
            <nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
                <div class="cm-flex">
                    <div class="nav-tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#">Painel de Monitoramento</a></li>
                            <li><a href="components-buttons.html">Cadastrar</a></li>
                            <li><a href="components-interactive.html">Mudar Etapa</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div id="global" style=" margin-top: 45px;">
            <div class="container-fluid">
  
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="panel panel-default"> 
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><code>tr</code> classes</th>
                                        <th>Column heading</th>
                                        <th>Column heading</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="active">
                                        <th scope="row">1</th>
                                        <td>active</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr class="active">
                                        <th scope="row">3</th>
                                        <td>success</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr class="active">
                                        <th scope="row">5</th>
                                        <td>info</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">6</th>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr class="active">
                                        <th scope="row">7</th>
                                        <td>warning</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">8</th>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                    <tr class="active">
                                        <th scope="row">9</th>
                                        <td>danger</td>
                                        <td>Column content</td>
                                        <td>Column content</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
            
            
            
            