
            <nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
                <div class="cm-flex">
                    <div class="nav-tabs-container">
                        <ul class="nav nav-tabs">
                            
                            <li class="active"><a href="<?php echo base_url() ?>status/cadastro">Cadastrar</a></li>
                            <li ><a href="<?php echo base_url() ?>status">Pesquisar / Editar</a></li>
                            <!--<li><a href="components-interactive.html">Mudar Etapa</a></li>-->
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div id="global" style=" margin-top: 45px;">
            <div class="container-fluid">
 <div class="row cm-fix-height">
     
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Cadastro de Status</div>
                            <div class="panel-body" style="min-height: 214px;">
                                <?php if($msg==1){ ?>
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4">
                                        <div class="alert alert-info alert-dismissible fade in shadowed" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-fw fa-info-circle"></i> Salvo com Sucesso. 
                                        </div>
                                    </div>
                                    <div class="col-lg-4"></div>
                                </div>
                                <?php } ?>
                                    <form class="form-horizontal" method="post"  action="<?php echo base_url() ?>status/cadastrar" data-parsley-validate="">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Codigo</label>
                                            <div class="col-sm-2">
                                                <input type="text" readonly="" class="form-control" value="<?php echo $query[0]->CD_STATUS ?>" style=" font-weight: bold;" id="codigo" name="codigo" placeholder="Codigo">
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Descrição</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" id="nome" required="" value="<?php echo $query[0]->NM_STATUS ?>" name="nome" placeholder="Descrição">
                                            </div>
                                        </div>
                                        <div class="form-group" style=" ">
                                            <label for="inputEmail3" class="col-sm-3 control-label" >Situação</label>  
                                            <div class="col-sm-2">
                                                <select class="form-control" required="" name="situacao" id="situacao">
                                                 <option value="">...</option>
                                                 <option value="A" <?php if($query[0]->SITUACAO=='A'){ echo 'selected=""'; } ?> >EM ANDAMENTO</option> 
                                                 <option value="C" <?php if($query[0]->SITUACAO=='C'){ echo 'selected=""'; } ?>>CANCELADO</option>
                                                 <option value="O" <?php if($query[0]->SITUACAO=='O'){ echo 'selected=""'; } ?>>CONCLUÍDO</option>
                                              </select> 
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Quantidade</label>
                                            <div class="col-sm-2">
                                                <input type="number" class="form-control" required="" value="<?php echo $query[0]->QUANT_DIA ?>" id="quant" name="quant" placeholder="Quantidade">
                                            </div>
                                        </div>
                                        
                                        <?php if($query[0]->CD_STATUS){ ?>
                                        <div class="form-group" style=" ">
                                            <label for="inputEmail3" class="col-sm-3 control-label" >Ativo</label>  
                                            <div class="col-sm-2">
                                                <select class="form-control" required="" name="ativo" id="ativo">
                                                 <option value="">...</option>
                                                 <option value="S"  <?php if($query[0]->SN_ATIVO=='S'){ echo 'selected=""'; } ?> >ATIVO</option> 
                                                 <option value="N" <?php if($query[0]->SN_ATIVO=='N'){ echo 'selected=""'; } ?>>INATIVO</option> 
                                              </select> 
                                            </div>
                                        </div>
                                        <?php } ?>
                                    
                                    <div class="row">
                                        <div class="col-sm-3" style=" "></div>
                                       
                                        
                                        <div class="col-sm-3" style=" ">
                                            <div class="form-group" style=" ">
                                                <div class="  text-left">
                                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            
            
            
            