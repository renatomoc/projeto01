
            <nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
                <div class="cm-flex">
                    <div class="nav-tabs-container">
                        <ul class="nav nav-tabs">
                            <li><a href="<?php echo base_url() ?>status/cadastro">Cadastrar</a></li>
                            <li class="active" ><a href="<?php echo base_url() ?>status">Pesquisar / Editar</a></li>
                            <!--<li><a href="components-interactive.html">Mudar Etapa</a></li>-->
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div id="global" style="  ">
            <div class="container-fluid">
  
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="panel panel-default"> 
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th> Descrição</th>
                                        <th>Quantidade Dias</th>
                                        <th>Situação</th> 
                                        <th style=" width: 50px; text-align: center;">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $x=0; foreach ($query as $val) {
                                        $situacao="";
                                        if($val->SITUACAO=='A'){ $situacao='EM ANDAMENTO'; }
                                        if($val->SITUACAO=='C'){ $situacao='CANCELADO'; }
                                        if($val->SITUACAO=='O'){ $situacao='CONCLUIDO'; }
                                        ?>
                                    <tr >
                                        <th scope="row"><?php echo $val->CD_STATUS ?></th>
                                        <td><?php echo $val->NM_STATUS ?></td>
                                        <td><?php echo $val->QUANT_DIA ?></td>
                                        <td><?php echo $situacao?></td> 
                                        <td style=" text-align: center;">
                                            <a href="<?php echo base_url() ?>status/cadastro/<?php echo $val->CD_STATUS  ?>">
                                            <img height="21" src="<?php echo base_url() ?>style\img\note.png"> 
                                            </a>
                                        </td>
                                    </tr> 
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
            
            
            
            