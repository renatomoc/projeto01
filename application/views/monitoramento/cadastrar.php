
            <nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
                <div class="cm-flex">
                    <div class="nav-tabs-container">
                        <ul class="nav nav-tabs">
                            <li ><a href="<?php echo base_url() ?>monitoramento">Painel de Monitoramento</a></li>
                            <li class="active"><a href="<?php echo base_url() ?>monitoramento/cadastro">Cadastrar</a></li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div id="global" style=" margin-top: 45px;">
            <div class="container-fluid">
 <div class="row cm-fix-height">
 
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Cadastro de Processo</div>
                            <div class="panel-body" style="min-height: 214px;">
                                <?php if($msg==1){ ?>
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4">
                                        <div class="alert alert-info alert-dismissible fade in shadowed" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-fw fa-info-circle"></i> Salvo com Sucesso 
                                        </div>
                                    </div>
                                    <div class="col-lg-4"></div>
                                </div>
                                <?php } ?>
                                <form class="form-horizontal" method="post"  action="<?php echo base_url() ?>monitoramento/cadastrar" data-parsley-validate="">
                                    <div class="row">
                                        <div class="col-sm-1" style=" "></div>
                                        
                                        <div class="col-sm-2" style=" margin-right: 10px;">
                                            <div class="form-group" style=" margin-bottom: 5px;">
                                                <label for="inputEmail3" class="  control-label" style=" text-align: left;">Código</label>  
                                                <input type="text" class="form-control" id="codigo" name="codigo" required="" readonly="" placeholder="Codigo">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group" style=" margin-right: 10px;  margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Data</label>  
                                                <input type="date" class="form-control" id="data" required="" name="data" placeholder="Data">
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group" style=" margin-right: 10px;  margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Data Chegada</label>  
                                                <input type="date" class="form-control" id="dt_chegada" required="" name="dt_chegada" placeholder="Data Chegada">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group" style=" margin-right: 10px; margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Quantidade Vaga</label>  
                                                <input type="text" class="form-control" id="quant" required="" name="quant" placeholder="Quantidade">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group" style="  margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Num. Requisição</label>  
                                                <input type="text" class="form-control" id="requisicao" required="" name="requisicao" placeholder="Requisição">
                                            </div>
                                        </div>
                                        <div class="col-sm-1" style=" "></div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-1" style=" "></div>
                                        
                                        <div class="col-sm-4" style=" margin-right: 10px;">
                                            <div class="form-group" style=" margin-bottom: 5px;">
                                                <label for="inputEmail3" class="  control-label" style=" text-align: left;">Cargo</label>  
                                                <select class="form-control" name="cargo" id="cargo">
                                                    <option value="">...</option>
                                                    <?php
                                                    foreach ($Lcargo as $val) { ?>
                                                        <option value="<?php echo $val->CACODCARGO ?>"><?php echo $val->CADESCARGO ?></option> 
                                                    <?php } ?>
                                                </select> 
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group" style=" margin-right: 10px;  margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Sexo</label>  
                                                <select class="form-control" name="sexo" id="sexo">
                                                    <option value="">...</option>
                                                    <option value="A">AMBOS</option>
                                                    <option value="F">FEMININO</option>
                                                    <option value="M">MASCULINO</option>
                                                </select> 
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group" style="  margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Tipo Requisição</label>  
                                                <select class="form-control" name="tp_requisicao" id="tp_requisicao">
                                                    <option value="">...</option>
                                                    <option value="A">AUMENTO DE QUADRO</option> 
                                                    <option value="S">SUBSTITUIÇÃO</option>
                                                </select> 
                                            </div>
                                        </div> 
                                        <div class="col-sm-1" style=" "></div>
                                    </div>
                                     
                                     <div class="row">
                                        <div class="col-sm-1" style=" "></div>
                                        
                                        <div class="col-sm-4" style=" margin-right: 10px;">
                                            <div class="form-group" style=" margin-bottom: 5px;">
                                                <label for="inputEmail3" class="  control-label" style=" text-align: left;">Setor</label>  
                                                <select class="form-control" required="" name="setor" id="setor">
                                                    <option value="">...</option> 
                                                    <?php
                                                    foreach ($Lsetor as $val) { ?>
                                                        <option value="<?php echo $val->CCCODIGO ?>"><?php echo $val->CCDESCRIC ?></option> 
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group" style=" margin-right: 10px;  margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Responsavel</label>  
                                                <select class="form-control" required="" name="responsavel" id="responsavel">
                                                    <option value="">...</option> 
                                                    <?php
                                                    foreach ($Lfunc as $val) { ?>
                                                        <option value="<?php echo $val->FUMATFUNC ?>"><?php echo $val->FUNOMFUNC ?></option> 
                                                    <?php } ?>
                                                </select> 
                                                
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group" style="  margin-bottom: 5px;">
                                                <label for="inputEmail3" class=" control-label" style=" text-align: left;">Status</label>  
                                                <select class="form-control" required="" name="status" id="status">
                                                    <option value="">...</option>  
                                                    <?php
                                                    foreach ($Lstatus as $val) { ?>
                                                        <option value="<?php echo $val->CD_STATUS ?>"><?php echo strtoupper ($val->NM_STATUS) ?></option> 
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div> 
                                        <div class="col-sm-1" style=" "></div>
                                    </div>
                                    
                                     
                                    <div class="row">
                                        <div class="col-sm-8" style=" "></div>
                                       
                                        
                                        <div class="col-sm-3" style=" ">
                                            <div class="form-group" style=" ">
                                                <div class="  text-right">
                                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-1" style=" "></div>
                                    </div>
                                    
                                    

                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            
            
            
            