<script src="<?php echo base_url() ?>style/assets/js/lib/jquery-2.1.3.min.js"></script>
<script type="text/javascript">
 
    Modal = function (cod) {
        document.getElementById('retorno_modal').innerHTML = "<div style='text-align: center;'><br><br><br><br><br><br><br><br><img height='140' src='<?php echo base_url() ?>style/img/Carregando2.gif'><br><FONT face=verdana color=#94d6ef size=3><B>CARREGANDO DADOS...<BR/><BR/><BR/></B></FONT></div>";
        $.post("<?php echo base_url() ?>monitoramento/modal", {cod: cod },
                function (pegar_dados) {
                    complete:$("#retorno_modal").empty().html(pegar_dados);
                }
        );
    }
    
    ModalGrav = function (cod) {
        var Status = $("#status").val(); 
        var Resp = $("#responsavel").val(); 
        var observ = $("#observ").val(); 
         
        document.getElementById('retorno_modal').innerHTML = "<div style='text-align: center;'><br><br><br><br><br><br><br><br><img height='140' src='<?php echo base_url() ?>style/img/Carregando2.gif'><br><FONT face=verdana color=#94d6ef size=3><B>CARREGANDO DADOS...<BR/><BR/><BR/></B></FONT></div>";
        $.post("<?php echo base_url() ?>monitoramento/modal", {cod: cod,Status:Status,Resp:Resp,observ:observ,tipo:'I' },
                function (pegar_dados) {
                    complete:$("#retorno_modal").empty().html(pegar_dados);
                }
        );
    }
    function blink(selector) {
        $(selector).fadeOut('slow', function() {
            $(this).fadeIn('slow', function() {
                blink(this);
            });
        });
    }

   
    
</script> 
<script type="text/javascript">
    function blink(selector) {
        $(selector).fadeOut('slow', function() {
            $(this).fadeIn('slow', function () {
                blink(this);
            });
        });
    }

     blink('.piscarr');    
</script>
<style type="text/css">
    .red{ color: red; font-weight: bold; }
</style>
 
            <nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
                <div class="cm-flex">
                    <div class="nav-tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="<?php echo base_url() ?>monitoramento">Painel de Monitoramento</a></li>
                            <li><a href="<?php echo base_url() ?>monitoramento/cadastro">Cadastrar</a></li>
                            <li style=" text-align: right; width: 100%;"><img data-toggle="modal" style=" cursor: pointer; margin-right: 15px;" data-target="#parametros" height="32" src="<?php echo base_url() ?>style\img\search.png"> </li> 
                        </ul>
                    </div>
                </div>
            </nav>



        </header>
        <div id="global" style=" ">
            <div class="container-fluid">
  

                
                
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="panel panel-default"> 
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Data</th>
                                        <th>Requisição</th>
                                        <th>Setor</th>
                                        <th>Cargo</th>
                                        <th>Responsavel</th>
                                        <th>Status</th>
                                        <th>Dt.Status</th>
                                        <th style=" text-align: center;">Dias Status</th>
                                        <th style=" text-align: center;">Total Dias</th>
                                        <th style=" text-align: center;">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $x=0; foreach ($query as $val) {
                                        $situacao="";
                                        if($val->SITUACAO=='A'){ $situacao='EM ANDAMENTO'; }
                                        if($val->SITUACAO=='C'){ $situacao='CANCELADO'; }
                                        if($val->SITUACAO=='O'){ $situacao='CONCLUIDO'; }
                                        $cla="";
                                        if($val->NR_DIA > $val->QUANT_DIA){ $cla="red"; }
                                        ?>
                                    <tr>
                                        <th scope="row"><?php echo str_pad($val->CD_MOV_RECRUTAMENTO, 4 , '0' , STR_PAD_LEFT); ?></th>
                                        <td><?php echo $val->DT_REQUISICAO ?></td>
                                        <td><?php echo $val->NUM_REQUISICAO ?></td>
                                        <td><?php echo substr($val->CCDESCRIC,0,25); ?></td>
                                        <td><?php echo substr($val->CADESCARGO,0,20); ?></td>
                                        <td><?php echo substr($val->FUNOMFUNC,0,20); ?></td>
                                        <td> <div data-toggle="tooltip" class="tooltip-test" data-placement="top" title="" data-original-title="Limite :  <?php echo $val->QUANT_DIA ?>"><?php echo $val->NM_STATUS ?></div></td>
                                        <td><?php echo $val->DT_STATUS; ?></td>
                                        <td style=" text-align: center;" class="<?php echo $cla; ?>"><?php echo $val->NR_DIA ?></td>
                                        <td style=" text-align: center;"><?php echo $val->NR_DIA_TOTAL ?></td>
                                        <td style=" text-align: center;"><img data-toggle="modal" style=" cursor: pointer;" onclick="Modal('<?php echo $val->CD_MOV_RECRUTAMENTO ?>');" data-target="#myModal" height="20" src="<?php echo base_url() ?>style\img\list.png"> </td>
                                    </tr>  
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
                        <style type="text/css">
         
 
.modal_detalhes .font-roboto {
  font-family: 'roboto condensed';
}

* {
  box-sizing: border-box;
}



.modal_detalhes .modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  overflow: hidden;
}

.modal_detalhes .modal-dialog {
  position: fixed;
  margin: 0;
  width: 100%;
  height: 100%;
  padding: 0;
}

.modal_detalhes .modal-content {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  border: 2px solid #3c7dcf;
  border-radius: 0;
  box-shadow: none;
}

.modal_detalhes .modal-header {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  height: 50px;
  padding: 10px;
  background:#307ecc;
  border: 0;
}

.modal_detalhes .modal-title {
  font-weight: 300;
  font-size: 2em;
  color: #fff;
  line-height: 30px;
}

.modal_detalhes .modal-body {
  position: absolute;
  top: 50px;
  bottom: 60px;
  width: 100%;
  font-weight: 300;
  overflow: auto;
}

.modal_detalhes .modal-footer {
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  height: 60px;
  padding: 10px;
  background: #f1f3f5;

  &:focus,
  &:active,
  &:active:focus {
    box-shadow: none;
    outline: none;
  }
}

.modal_detalhes .btn-modal {
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -20px;
  margin-left: -100px;
  width: 200px;
}
 







p {
  font-size: 1.4em;
  line-height: 1.5;
  color: lighten(#5f6377, 20%);

  &:last-child {
    margin-bottom: 0;
  }
}

::-webkit-scrollbar {
  -webkit-appearance: none;
  width: 10px;
  background: #f1f3f5;
  border-left: 1px solid darken(#f1f3f5, 10%);
}

::-webkit-scrollbar-thumb {
  background: darken(#f1f3f5, 20%);
}
                     
                            
</style>        
<div class="modal_detalhes">            
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div id="retorno_modal"> </div>
                       <!-- <img src="<?php echo base_url() ?>style\img\carregando.gif">   -->
                    </div>
                    
                </div>
                <!-- /.modal-dialog -->
            </div>
</div>

<div id="parametros" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" style="color: white; font-size: 35px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    PARAMETROS DE PESQUISA
                    <a class="anchorjs-link" href="#myModalLabel"><span class="anchorjs-icon"></span></a>
                </h4>
            </div>
            <form class="form-horizontal" method="post"  action="<?php echo base_url() ?>monitoramento" data-parsley-validate=""> 
            <div class="modal-body">  
                <div class="row"> 
 
                        <div class="row">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label" style=" text-align: right; padding-top: 8px;">Status</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="status" id="status">
                                        <option value="">...</option>
                                        <?php foreach ($Lstatus as $val) { ?>
                                            <option value="<?php echo $val->CD_STATUS ?>"><?php echo strtoupper($val->NM_STATUS) ?></option> 
                                        <?php } ?>
                                    </select> 
                                </div>
                            </div>
                        </div> 
                   
                        <div class="row">
                            <div class="form-group" style="margin-top: 10px;">
                                <label for="inputEmail3" class="col-sm-3 control-label" style=" text-align: right; ">Cargo</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="cargo" id="cargo">
                                        <option value="">...</option>
                                                    <?php
                                                    foreach ($Lcargo as $val) { ?>
                                                        <option value="<?php echo $val->CACODCARGO ?>"><?php echo $val->CADESCARGO ?></option> 
                                                    <?php } ?>
                                    </select> 
                                </div>
                            </div>
                        </div> 
                    
                        <div class="row">
                            <div class="form-group" style="margin-top: 10px;">
                                <label for="inputEmail3" class="col-sm-3 control-label" style=" text-align: right; ">Setor</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="setor" id="setor">
                                        <option value="">...</option>
                                                    <?php
                                                    foreach ($Lsetor as $val) { ?>
                                                        <option value="<?php echo $val->CCCODIGO ?>"><?php echo $val->CCDESCRIC ?></option> 
                                                    <?php } ?>
                                    </select> 
                                </div>
                            </div>
                        </div> 
                    
                        <div class="row">
                            <div class="form-group" style="margin-top: 10px;">
                                <label for="inputEmail3" class="col-sm-3 control-label" style=" text-align: right; ">Responsavel</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="respon" id="respon">
                                        <option value="">...</option>
                                                    <?php
                                                    foreach ($Lfunc as $val) { ?>
                                                        <option value="<?php echo $val->FUMATFUNC ?>"><?php echo $val->FUNOMFUNC ?></option> 
                                                    <?php } ?>
                                    </select> 
                                </div>
                            </div>
                        </div> 
                    
                </div> 
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Pesquisar</button>
            </div>
            </form>
        </div>

    </div>
    <!-- /.modal-dialog -->
</div>