              
<div class="modal-header">
    <button type="button" class="close" style="color: white; font-size: 35px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <h4 class="modal-title" id="myModalLabel">
        PROCESSO <?php echo str_pad($query[0]->CD_MOV_RECRUTAMENTO, 4 , '0' , STR_PAD_LEFT); ?>
        <a class="anchorjs-link" href="#myModalLabel"><span class="anchorjs-icon"></span></a>
    </h4>
</div>
<div class="modal-body"> 
    <table class="table table-striped">
        <tbody>
            <tr >
                <td style=" width: 25%;"><label for="inputEmail3" class="control-label" >DATA REQUISIÇÃO: </label> <?php echo $query[0]->DT_REQUISICAO; ?></td>
                <td style=" width: 25%;"><label for="inputEmail3" class="control-label" >DATA CHEGADA: </label> <?php echo $query[0]->DT_CHEGADA; ?></td>
                <td style=" width: 25%;"><label for="inputEmail3" class="control-label" >QUANTIDADE DE VAGA: </label>  <?php echo $query[0]->QUANT_VAGA; ?></td>
                <td style=" width: 25%;"><label for="inputEmail3" class="control-label" >N° REQUISIÇÃO: </label> <?php echo $query[0]->NUM_REQUISICAO; ?></td>
            </tr>
            <tr >
                <td style=" width: 50%;" colspan="2"><label for="inputEmail3" class="control-label" >CARGO: </label> <?php echo $query[0]->CADESCARGO; ?></td>
                <td style=" width: 25%;"><label for="inputEmail3" class="control-label" >SEXO: </label> <?php echo $query[0]->SEXO; ?></td>
                <td style=" width: 25%;"><label for="inputEmail3" class="control-label" >TP REQUISIÇÃO: </label> <?php echo $query[0]->TP_REQUISICAO; ?></td>
            </tr>
            <tr >

                <td style=" width: 100%;" colspan="4">
                    <div class="row">
                        <div class="col-sm-4"><label for="inputEmail3" class="control-label" >SETOR: </label> <?php echo $query[0]->CCDESCRIC; ?></div>
                        <div class="col-sm-4"><label for="inputEmail3" class="control-label" >RESPONSÁVEL: </label> <?php echo $query[0]->FUNOMFUNC; ?></div>
                        <div class="col-sm-4"><label for="inputEmail3" class="control-label" >STATUS: </label> <?php echo $query[0]->NM_STATUS; ?></div>
                    </div>
                </td>

            </tr>
        </tbody>
    </table> 

                                <?php if($msg==1){ ?>
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4">
                                        <div class="alert alert-info alert-dismissible fade in shadowed" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-fw fa-info-circle"></i> Salvo com Sucesso 
                                        </div>
                                    </div>
                                    <div class="col-lg-4"></div>
                                </div>
                                <?php } ?>
                                <?php if($msg==2){ ?>
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4">
                                        <div class="alert alert-danger alert-dismissible fade in shadowed" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-fw fa-info-circle"></i> Erro ao gravar Cadastro
                                        </div>
                                    </div>
                                    <div class="col-lg-4"></div>
                                </div>
                                <?php } ?>
    <div class="row"> 

        <div class="col-sm-5">
            <div class="row">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label" style=" text-align: right; padding-top: 8px;">Status</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="status" id="status">
                            <option value="">...</option>
                               <?php
                                 foreach ($Lstatus as $val) { ?>
                                    <option value="<?php echo $val->CD_STATUS ?>"><?php echo strtoupper ($val->NM_STATUS) ?></option> 
                               <?php } ?>
                        </select> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group" style=" margin-top: 12px;">
                    <label for="inputEmail3" class="col-sm-3 control-label" style=" text-align: right; padding-top: 8px;">Responsável</label>
                    <div class="col-sm-9">
                                                <select class="form-control" required="" name="responsavel" id="responsavel">
                                                    <option value="">...</option> 
                                                    <?php
                                                    foreach ($Lfunc as $val) { ?>
                                                        <option value="<?php echo $val->FUMATFUNC ?>"><?php echo $val->FUNOMFUNC ?></option> 
                                                    <?php } ?>
                                                </select>  
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12" style=" margin-right: 10px;">
                    <div class="form-group" style=" margin-bottom: 5px;"> 
                        <textarea name="observ" id="observ" class="form-control" rows="3" ></textarea>
                        </select> 
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-sm-1" style=" ">

        </div>
    </div>
    <div class="row">
        <div class="col-sm-8" style=" "></div>


        <div class="col-sm-3" style=" ">
            <div class="form-group" style=" ">
                <div class="  text-right">
                    <button type="button" onclick="ModalGrav('<?php echo $cod ?>');" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </div>
        <div class="col-sm-1" style=" "></div>
    </div>
    <hr>
    <h4 id="tooltips-in-a-modal">
        Historico 
    </h4>
    <div class="row"> 
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="panel panel-default"> 
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Status</th>
                            <th style=" text-align: center;">Data Incio</th>
                            <th style=" text-align: center;">Data Fim</th>
                            <th style=" text-align: center;">Tempo</th>
                            <th style=" text-align: center;">Limite</th>
                            <th>Responsavel Status</th>
                            <th>Observação</th> 
                        </tr>
                    </thead>
                    <tbody>
                            <?php
                            $x = 0;
                            foreach ($queryy as $val) { 
                                if($val->NR_DIA > $val->QUANT_DIA){ $cla="red"; }
                            ?>
                            <tr>
                                <th scope="row"><?php echo strtoupper($val->CD_USUARIO) ?></th>
                                <td><?php echo strtoupper($val->NM_STATUS) ?></td>
                                <td style=" text-align: center;"><?php echo $val->DT_CADASTRO ?></td>
                                <td style=" text-align: center;"><?php echo $val->DT_FIM ?></td>
                                <td style=" text-align: center;" class="<?php echo $cla; ?>"><?php echo $val->NR_DIA ?></td>
                                <td style=" text-align: center;" ><?php echo $val->QUANT_DIA ?></td>
                                <td><?php echo strtoupper($val->FUNOMFUNC) ?></td>
                                <td><?php echo strtoupper($val->OBS) ?></td>
                            </tr>  
<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div> 
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    <!--<button type="button" class="btn btn-primary">Salvar</button>-->
</div>
