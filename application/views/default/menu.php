            
            <div id="cm-menu-content">
                <div id="cm-menu-items-wrapper">
                    <div id="cm-menu-scroller">
                        <ul class="cm-menu-items">
                            <li><a href="index.html" class="sf-house">Inicio</a></li> 
                            <li class="cm-submenu">
                                <a class="sf-user-male">Monitoramento <span class="caret"></span></a>
                                <ul>
                                    <li><a href="<?php echo base_url() ?>monitoramento">Recrutamento e Seleção</a></li>
                                </ul>
                            </li> 
                            <li class="cm-submenu">
                                <a class="sf-wrench-screwdriver">Tabelas <span class="caret"></span></a>
                                <ul>
                                    <li><a href="<?php echo base_url() ?>status/cadastro">Status Vaga</a></li>
                                </ul>
                            </li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <header id="cm-header">
            <nav class="cm-navbar cm-navbar-primary">
                <div class="btn btn-primary md-menu-white hidden-md hidden-lg" data-toggle="cm-menu"></div>
                <div class="cm-flex">
                    <h1><img src="<?php echo base_url() ?>style/img/logo22.png" height="25"> Recrutamento e Seleção</h1> 
                  
                </div> 
                <div class="dropdown pull-right">
                    <button class="btn btn-primary md-account-circle-white" data-toggle="dropdown"></button>
                    <ul class="dropdown-menu">
                        <li class="disabled text-center">
                            <a style="cursor:default;"><strong>Renato</strong></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-cog"></i> Settings</a>
                        </li>
                        <li>
                            <a href="login.html"><i class="fa fa-fw fa-sign-out"></i> Sign out</a>
                        </li>
                    </ul>
                </div>
            </nav>