<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        
                <link rel="stylesheet" href="<?php echo base_url() ?>style/datepiker/assets/css/bootstrap.css">
                <link rel="stylesheet" href="<?php echo base_url() ?>style/datepiker/assets/datepicker/css/bootstrap-datepicker.css">
                             
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>style/assets/css/bootstrap-clearmin.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>style/assets/css/roboto.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>style/assets/css/material-design.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>style/assets/css/small-n-flat.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>style/assets/css/font-awesome.min.css">

        <title>:::. Santa Casa - RH .:::</title>
    </head>
    <style type="text/css">
        .titulo{
            line-height: 50px;
            font-size: 22px;
            font-weight: bold;
            margin: 0;
            padding: 0;
            overflow: hidden;
            text-overflow: ellipsis;
            text-align: center;
            white-space: nowrap;
        }
    </style>
    <body class="cm-no-transition cm-1-navbar cm-menu-toggled">
        <div id="cm-menu">
            <nav class="cm-navbar cm-navbar-primary">
                <div class="cm-flex titulo">Santa Casa</div>
                <div class="btn btn-primary md-menu-white" data-toggle="cm-menu"></div>
            </nav>