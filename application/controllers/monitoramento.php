<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoramento extends CI_Controller {
        function __construct() {
            // Call the Controller constructor
            parent::__construct();
            $this->load->model('geral_model'); 
        }

    public function index() { 
	      $dados['msg']=$this->session->userdata('MSG');
              $dados['msg2']=$this->session->userdata('MSG2');
              $data = array(
                     'MSG' => '',
                     'MSG2' => ''
              );
              $this->session->set_userdata($data);
              $dados['Lsetor']= $this->geral_model->GetSetor(); 
              $dados['Lfunc']= $this->geral_model->GetFuncionario(); 
              $dados['Lcargo']= $this->geral_model->GetCargo(); 
              $dados['Lstatus']= $this->geral_model->GetStatus();
              
              $dados['query']= $this->geral_model->GetMovimento($this->input->post('status'),$this->input->post('cargo'),$this->input->post('setor'),$this->input->post('respon')); 
              $this->montaPagina("monitoramento/painel",$dados);
    }
        
    public function cadastro() { 
	      $dados['msg']=$this->session->userdata('MSG');
              $dados['msg2']=$this->session->userdata('MSG2');
              $data = array(
                     'MSG' => '',
                     'MSG2' => ''
              );
              $this->session->set_userdata($data);
              $dados['Lsetor']= $this->geral_model->GetSetor(); 
              $dados['Lfunc']= $this->geral_model->GetFuncionario(); 
              $dados['Lcargo']= $this->geral_model->GetCargo(); 
              $dados['Lstatus']= $this->geral_model->GetStatus(); 
              
              $this->montaPagina("monitoramento/cadastrar",$dados);
    }

    function cadastrar() {	
                        $DtChegada=explode('-',$this->input->post('dt_chegada'));
                        $DtRequisicao=explode('-',$this->input->post('data'));
			if(!$this->input->post('codigo')){
                                
				$dadoss = array(  
                                        'DT_REQUISICAO' =>$DtRequisicao[2]."-".$DtRequisicao[1]."-".$DtRequisicao[0],
                                        'DT_CHEGADA' =>$DtChegada[2]."-".$DtChegada[1]."-".$DtChegada[0],
                                        'QUANT_VAGA' =>$this->input->post('quant'),
                                        'NUM_REQUISICAO' =>$this->input->post('requisicao'),
                                        'CD_CARGO' =>$this->input->post('cargo'),
                                        'SEXO' =>$this->input->post('sexo'),
                                        'TP_REQUISICAO' =>$this->input->post('tp_requisicao'),
                                        'CD_SETOR' =>$this->input->post('setor'),
                                        'CD_RESPONSAVEL' =>$this->input->post('responsavel'),
                                        'CD_STATUS' =>$this->input->post('status'), 
                                        'DT_CADASTRO' =>$this->geral_model->DataAtual(),
                                        'SN_ATIVO' =>'S',
                                        'CD_USUARIO' =>NULL,
                                        'CD_USUARIO_STATUS' =>NULL,
                                        'DT_STATUS' =>$this->geral_model->DataAtual(), 
				);	                                
				$retorno=$this->geral_model->inserir('DBAINFO.RH_MOV_RECRUTAMENTO',$dadoss);
                                if($retorno==1){
                                    
                                    $dados['Codigo']=$this->geral_model->UltimoCod();
                                    $dadoss = array(  
                                            'CD_MOV_RECRUTAMENTO' =>$dados['Codigo'],
                                            'CD_STATUS' =>$this->input->post('status'), 
                                            'DT_CADASTRO' =>$this->geral_model->DataAtual(),
                                            'CD_RESPONSAVEL' =>$this->input->post('responsavel'),
                                            'CD_USUARIO' =>NULL,
                                            'OBS' =>'CADASTRO DA VAGA NO SISTEMA'
                                    );	                                
                                    $retorno=$this->geral_model->inserir('DBAINFO.RH_ITMOV_RECRUTAMENTO',$dadoss);
                                    
                                } 
                                
				if($retorno)
				$dados['msg']=1;
				else
				$dados['msg2']=1;
			}else{ 
			}
                  
                        $data = array(
                                 'MSG' => $dados['msg'],
                                 'MSG2' => $dados['msg2'],
                        );
                       
                        $this->session->set_userdata($data);
                        redirect(base_url().'monitoramento/cadastro',$dados);  

			
			
	}
        
    function modal() {	
        $dados['cod']=$this->input->post('cod');  
        $dados['Status']=$this->input->post('Status'); 
        $dados['Resp']=$this->input->post('Resp'); 
        $dados['observ']=$this->input->post('observ'); 
        $dados['tipo']=$this->input->post('tipo'); 
        if($dados['tipo']=='I'){
            $Erro=0;
            if(empty($dados['cod'])){ $Erro=1; }
            if(empty($dados['Status'])){ $Erro=1; }
            if(empty($dados['Resp'])){ $Erro=1; } 
            echo $Erro;
            if($Erro==0){ 
                $ItCodigo=$this->geral_model->CodUltimaItMov($dados['cod']);
                
                $dadoss = array(  
                    'CD_MOV_RECRUTAMENTO' =>$dados['cod'],
                    'CD_STATUS' =>$dados['Status'], 
                    'DT_CADASTRO' =>$this->geral_model->DataAtual(),
                    'CD_RESPONSAVEL' =>$dados['Resp'],
                    'CD_USUARIO' =>'USUARIO',
                    'OBS' =>strtoupper($dados['observ'])
                );	                                
                $retorno=$this->geral_model->inserir('DBAINFO.RH_ITMOV_RECRUTAMENTO',$dadoss);
                
                if($retorno=='1'){
                    
                    $dadoss = array(  
                        'CD_ITMOV_RECRUTAMENTO' =>$ItCodigo, 
                        'DT_FIM' =>$this->geral_model->DataAtual(), 
                    );	 
                   // print_r($dadoss);
                     
                    $retorno = $this->geral_model->atualizar('DBAINFO.RH_ITMOV_RECRUTAMENTO', $dadoss, "CD_ITMOV_RECRUTAMENTO");                 
                }
                
                if($retorno=='1'){
                    $dadoss = array(  
                        'CD_MOV_RECRUTAMENTO' =>$dados['cod'],
                        'CD_STATUS' =>$dados['Status'], 
                        'DT_STATUS' =>$this->geral_model->DataAtual(),
                        'CD_RESPONSAVEL' =>$dados['Resp'],
                        'CD_USUARIO_STATUS' =>'USUARIO'
                    );	                                 
                    $retorno = $this->geral_model->atualizar('DBAINFO.RH_MOV_RECRUTAMENTO', $dadoss, "CD_MOV_RECRUTAMENTO");
                    
                    if($retorno==1){
                        $dados['msg']=1;
                    }else{
                        $dados['msg']=2;
                    }
                }else{
                    $dados['msg']=2;
                } 
            }else{
                $dados['msg']=2;
            }

        }
        
        if($dados['cod']){
            $dados['query']= $this->geral_model->GetMovimentoModal($dados['cod']); 
            $dados['queryy']= $this->geral_model->GetItMovimentoModal($dados['cod']);
        }
        $dados['Lfunc']= $this->geral_model->GetFuncionario();
        $dados['Lstatus']= $this->geral_model->GetStatus(); 
	$this->load->view("monitoramento/query",$dados);  
    } 
}
