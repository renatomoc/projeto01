<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {
        function __construct() {
            // Call the Controller constructor
            parent::__construct();
            $this->load->model('geral_model'); 
        }

    public function index() { 
	      $dados['msg']=$this->session->userdata('MSG');
              $dados['msg2']=$this->session->userdata('MSG2');
              $data = array(
                     'MSG' => '',
                     'MSG2' => ''
              );
              $this->session->set_userdata($data);
              $dados['query']= $this->geral_model->GetStatus(); 
              //$dados['evento']= $this->geral_model->GetEvento(); 
              $this->montaPagina("status/painel",$dados);
    }
        
    public function cadastro() { 
	      $dados['msg']=$this->session->userdata('MSG');
              $dados['msg2']=$this->session->userdata('MSG2');
              $data = array(
                     'MSG' => '',
                     'MSG2' => ''
              );
              $this->session->set_userdata($data);
              echo $Codigo= $this->uri->segment(3);
              if($Codigo){
                  $dados['query']= $this->geral_model->GetStatus($Codigo); 
                  //print_r($dados['query']);
              }
              
              $this->montaPagina("status/cadastrar",$dados);
    } 
    function cadastrar() {	
                        
			if(!$this->input->post('codigo')){
                                
				$dadoss = array(
					'NM_STATUS' =>strtoupper($this->input->post('nome')),
                                        'QUANT_DIA' =>$this->input->post('quant'),
                                        'SITUACAO' =>$this->input->post('situacao'),
                                        'SN_ATIVO' =>'S'
                                    
				);
                                //$retorno=$this->geral_model->testeaaa();
                                //print_r($retorno);
                                
				$retorno=$this->geral_model->inserir('DBAINFO.RH_STATUS',$dadoss);
				 
                                if($retorno)
				$dados['msg']=1;
				else
				$dados['msg2']=1;
			}else{
                                
				$dadoss = array(
                                        'CD_STATUS' =>$this->input->post('codigo'),
					'NM_STATUS' =>strtoupper ($this->input->post('nome')),
                                        'QUANT_DIA' =>$this->input->post('quant'),
                                        'SITUACAO' =>$this->input->post('situacao'),
                                        'SN_ATIVO' =>$this->input->post('ativo')
				);
                                //PRINT_R($dadoss );
                                $retorno=$this->geral_model->atualizar('DBAINFO.RH_STATUS',$dadoss,"CD_STATUS");   
				if($retorno)
				$dados['msg']=1;
				else
				$dados['msg2']=1;
			}
                  
                        $data = array(
                                 'MSG' => $dados['msg'],
                                 'MSG2' => $dados['msg2'],
                        );
                       
                        $this->session->set_userdata($data);
                        redirect(base_url().'status',$dados);  

			
			
	}
        
    function excluir() {	
                        
			if($this->input->post('CD_CONVENIO')){
                                
				$dadoss = array(
                                        'CD_CONVENIO' =>$this->input->post('CD_CONVENIO'),
					'NM_CONVENIO' =>$this->input->post('NM_CONVENIO'),
                                        'CD_EVENTO' =>$this->input->post('EVENTO'),
                                        'SN_ATIVO' =>'S',
                                        'DATA' =>date('d/m/Y'),
                                        'USUARIO' =>$this->session->userdata('CD_USUARIO')
				);
                                //PRINT_R($dadoss );
                                $retorno=$this->geral_model->deletar('DBAINFO.BOLETO_CONVENIO',$dadoss,"CD_CONVENIO");  
				if($retorno)
				$dados['msg']=1;
				else
				$dados['msg2']=1;
			}
                        $data = array(
                                 'MSG' => $dados['msg'],
                                 'MSG2' => $dados['msg2'],
                        );
                       
                        $this->session->set_userdata($data);
                        redirect(base_url().'convenio',$dados);  

			
			
	}
}
