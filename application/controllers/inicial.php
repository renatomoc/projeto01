<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicial extends CI_Controller {
 
    public function index() {
                        $dados['MenuCadastro']="active open";
                        $dados['MenuInicial']="active";
			$this->montaPagina("modelo");
    }
    
    public function teste() { 
        $this->montaPagina("inicial");
    }
    
    public function date() {  
       // $this->load->view("monitoramento/date",$dados);	
        $this->montaPagina("monitoramento/date",$dados);
    }
}
