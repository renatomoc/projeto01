<?php

class Geral_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->db->query("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI'");
    }

    public function inserir($tabela, $array = null) {

        $retorno = $this->db->insert($tabela, $array);
        return $this->db->affected_rows();;
    }

    public function atualizar($tabela, $array = null, $campo) {

        $this->db->where($campo, $array[$campo]);
        return $this->db->update($tabela, $array);
    }
    
    public function deletar($tabela, $array = null, $campo) {

        $this->db->where($campo, $array[$campo]);
        return $this->db->delete($tabela);
    }
       
    public function GetFuncionario() {
        $query = $this->db->query("  
            select  fumatfunc,funomfunc 
            from fpw.funciona where fucentrcus in ('0603','0602','0605')
            and fucodsitu in (select stcodsitu from fpw.situacao where sttipositu <> 'R') 
            order by 2  ");
        $retorno =  $query->result();
        return $retorno;
    }
    
    public function GetSetor() {
        $query = $this->db->query("  
        select cccodigo,ccdescric from fpw.cencusto
        where cccodigo not in (select cccodigo from fpw.cencusto where ccdescric like '%*%')
        and CCCODEMP=1
        order by 2 ");
        $retorno =  $query->result();
        return $retorno;
    }
    
    public function GetCargo() {
        $query = $this->db->query(" select distinct(cadescargo) cadescargo, cacodcargo 
         from fpw.cargos  WHERE CACODEMP=1 AND CATIPOSAL=1  order by 1 ");
        $retorno =  $query->result();
        return $retorno;
    }
    
    public function GetStatus($cod=null) {
        if($cod){ $cod=" AND CD_STATUS = $cod "; }
        $query = $this->db->query(" 
            SELECT /*CD_STATUS,NM_STATUS*/ *
            FROM DBAINFO.RH_STATUS
            WHERE SN_ATIVO='S'
            $cod
            ORDER BY 2 ");
        $retorno =  $query->result();
        return $retorno;
    }
    
    public function ListaEvento() {
        $query = $this->db->query("SELECT EVCODEVENT,EVDESCOMPL
        FROM FPW.EVENTOS,DBAINFO.FOLHA_EVENTOS
        WHERE EVCODEVENT = CD_EVENTO
        ORDER BY EVDESCOMPL ");
        $retorno =  $query->result();
        return $retorno;
    }
    
    public function GetGrupo() {
        $query = $this->db->query(" select * from dbainfo.folha_grupo order by cd_grupo desc ");
        $retorno =  $query->result();
        return $retorno;
    }
    
    
    public function GetMovimento($status=null,$cargo=null,$setor=null,$resp=null) {
        if($status){ $status=" AND RH_MOV_RECRUTAMENTO.CD_STATUS = $status "; }
        if($cargo){ $cargo=" AND CARGOS.CACODCARGO = $cargo "; }
        if($setor){ $setor=" AND RH_MOV_RECRUTAMENTO.CD_SETOR = $setor "; }
        if($resp){ $resp=" AND RH_MOV_RECRUTAMENTO.CD_RESPONSAVEL = $resp "; }
        $query = $this->db->query(" 
        SELECT CD_MOV_RECRUTAMENTO,TO_CHAR(DT_CHEGADA,'DD/MM/YYYY') DT_REQUISICAO,NUM_REQUISICAO,CCDESCRIC,
        CADESCARGO,FUNOMFUNC,NM_STATUS,TO_CHAR(DT_STATUS,'DD/MM/YYYY') DT_STATUS, 
        NVL(trunc(SYSDATE-DT_STATUS),'0') NR_DIA,NVL(QUANT_DIA, '0') QUANT_DIA, 
        NVL(trunc(SYSDATE-DT_CHEGADA),'0') NR_DIA_TOTAL
        FROM DBAINFO.RH_MOV_RECRUTAMENTO
        INNER JOIN DBAINFO.RH_STATUS ON RH_STATUS.CD_STATUS=RH_MOV_RECRUTAMENTO.CD_STATUS
        INNER JOIN FPW.FUNCIONA ON FUNCIONA.FUMATFUNC=RH_MOV_RECRUTAMENTO.CD_RESPONSAVEL
        INNER JOIN (SELECT * FROM FPW.CARGOS WHERE CACODEMP=1 AND CATIPOSAL=1) CARGOS ON CARGOS.CACODCARGO=RH_MOV_RECRUTAMENTO.CD_CARGO
        INNER JOIN (SELECT * FROM FPW.CENCUSTO WHERE CCCODEMP=1 ) CENCUSTO ON CENCUSTO.CCCODIGO=RH_MOV_RECRUTAMENTO.CD_SETOR
        WHERE RH_STATUS.SITUACAO ='A' $status $cargo $setor $resp 
        ORDER BY DT_REQUISICAO DESC "); 
        $retorno =  $query->result();
        return $retorno;
    }
    
    public function DataAtual() {
        $query = $this->db->query(" select sysdate DATA from dual ");
        $retorno =  $query->result();
        return $retorno[0]->DATA;
    }
    
    public function CodUltimaItMov($cod) {
        $query = $this->db->query(" SELECT MAX(CD_ITMOV_RECRUTAMENTO) COD FROM DBAINFO.RH_ITMOV_RECRUTAMENTO WHERE CD_MOV_RECRUTAMENTO = $cod ");
        $retorno =  $query->result();
        return $retorno[0]->COD;
    }
    
    public function UltimoCod() {
        $query = $this->db->query(" SELECT MAX(CD_MOV_RECRUTAMENTO) CODIGO FROM DBAINFO.RH_MOV_RECRUTAMENTO");
        $retorno =  $query->result();
        return $retorno[0]->CODIGO;
    }
    
    public function GetMovimentoModal($cod=null) {
        $query = $this->db->query("
            SELECT RH_MOV_RECRUTAMENTO.CD_MOV_RECRUTAMENTO,TO_CHAR(DT_REQUISICAO,'DD/MM/YYYY') DT_REQUISICAO,TO_CHAR(DT_CHEGADA,'DD/MM/YYYY') DT_CHEGADA,
            QUANT_VAGA,NUM_REQUISICAO,CADESCARGO,DECODE(SEXO,'A','AMBOS','F','FEMININO','M','MASCULINO',SEXO) SEXO, 
            DECODE(TP_REQUISICAO,'A','AUMENTO DE QUADRO','S','SUBSTITUIÇÃO') TP_REQUISICAO,CCDESCRIC,FUNOMFUNC,NM_STATUS
            FROM DBAINFO.RH_MOV_RECRUTAMENTO
            INNER JOIN (SELECT * FROM FPW.CARGOS WHERE CACODEMP=1 AND CATIPOSAL=1) CARGO ON CARGO.CACODCARGO=RH_MOV_RECRUTAMENTO.CD_CARGO
            INNER JOIN (SELECT * FROM FPW.CENCUSTO WHERE CCCODEMP=1 ) CENCUSTO ON CENCUSTO.CCCODIGO=RH_MOV_RECRUTAMENTO.CD_SETOR
            INNER JOIN FPW.FUNCIONA ON FUNCIONA.FUMATFUNC= RH_MOV_RECRUTAMENTO.CD_RESPONSAVEL
            INNER JOIN DBAINFO.RH_STATUS ON RH_STATUS.CD_STATUS=RH_MOV_RECRUTAMENTO.CD_STATUS
            WHERE RH_MOV_RECRUTAMENTO.CD_MOV_RECRUTAMENTO=$cod ");
        $retorno =  $query->result();
        return $retorno;
    }
    
     public function GetItMovimentoModal($cod=null) {
        $query = $this->db->query("
        SELECT TO_CHAR(DT_CADASTRO,'DD/MM/YYYY') DT_CADASTRO, OBS,NM_STATUS,FUNOMFUNC,
        RH_ITMOV_RECRUTAMENTO.CD_USUARIO,TO_CHAR(DT_FIM,'DD/MM/YYYY') DT_FIM,
        NVL(trunc(NVL(DT_FIM,SYSDATE)-DT_CADASTRO),'0') NR_DIA,NVL(RH_STATUS.QUANT_DIA, '0') QUANT_DIA
        FROM DBAINFO.RH_ITMOV_RECRUTAMENTO
        INNER JOIN DBAINFO.RH_STATUS ON RH_STATUS.CD_STATUS=RH_ITMOV_RECRUTAMENTO.CD_STATUS
        INNER JOIN FPW.FUNCIONA ON FUNCIONA.FUMATFUNC= RH_ITMOV_RECRUTAMENTO.CD_RESPONSAVEL
        WHERE RH_ITMOV_RECRUTAMENTO.CD_MOV_RECRUTAMENTO=$cod ORDER BY CD_ITMOV_RECRUTAMENTO DESC ");
        $retorno =  $query->result();
        return $retorno;
    }
 
 
   
    
    
    
    
    
     
 
	
}
